# README #

This Gulp setup will allow you to create new codeversions from your repository to Salesforce Commerce Cloud via Gulp. you can start it straight via Gulp or use this setup in Bamboo (atlassian).


### Using via Gulp / Terminal ###

* Run NPM install after checkout the repository.
* In the DW.json file you can setup your Salesforce connection details. You can also state if you are using TwoFactor authentication or not.

### TwoFactor Setup ###

First ask Salesforce for the TwoFactor certificates

**Step 1:**
Install OpenSSL

**Step 2:** 
Copy/unzip attached zip files in a new folder where openssl is accessible at path or otherwise copy into the openssl/bin folder directly and execute all the remaining commands in the same directory.

Run the following steps in Terminal

**Step 3.**
Replace $user with your SFCC username

openssl req -new -sha256 -newkey rsa:2048 -nodes -out $user.req -keyout $user.key

Note: change 'username' with your BM username

**Step 4.**
openssl x509 -CA cert.staging.yoursite.demandware.net_01.crt -CAkey cert.staging.yoursite.demandware.net_01.key -CAserial cert.staging.yoursite.demandware.net.srl -req -in $user.req -out $user.pem -days 365

When it asks for pass phrase open from the .txt file and copy/paste the value

**Step 5.**
openssl pkcs12 -export -in $user.pem -inkey $user.key -certfile cert.staging.yoursite.demandware.net_01.crt -name "$user" -out $user.p12

Store the Certificates in a folder (in the code it will look in the keystore folder. (this can be adjusted)

### Using Bamboo  ###

* For the use in Bamboo you will need to create the tasks for the following tasks: 

Your own tasks (JS / ScSS) before.
gulp:compress
gulp: build:deploy
gulp: buildcleanpackages

You will also need to add the following variables to the Bamboo setup.

* certPass
* CertPath
* hostname
* keyPath
* password
* two factor  (true/false)
* username
* activate (true/false)

### Any questions? ###

@paulvandrunen