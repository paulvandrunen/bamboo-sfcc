var gulp = require('gulp');
// Requires the gulp-sass plugin
var sass = require('gulp-sass');
var zip = require('gulp-zip');
var rename = require('gulp-rename');
var uploadconfig = require('@tridnguyen/config');
var credentials = uploadconfig('dw.json', {caller: false});
var minimist = require('minimist');
var opts = minimist(process.argv.slice(2));
var exec = require('child_process').exec;

//Build related configs
var build = {};
build.hostname          = opts.hostname || credentials.hostname;
build.username          = opts.username || credentials.username;
build.password          = opts.password || credentials.password;
build.codeVersion       = opts.codeVersion || credentials.codeVersion;
build.twofactor         = opts.twofactor || credentials.twofactor;
build.activate          = opts.activate || credentials.activate;
build.keyPath           = opts.keyPath || credentials.keyPath;
build.certPath          = opts.certPath || credentials.certPath;
build.certPass          = opts.certPass || credentials.certPass;

//Tasks

// Compress cartridges
gulp.task('compress',  function() {
    return gulp.src('./cartridges/**')
        .pipe(rename(function(file) {
            file.dirname = build.codeVersion + '/' + file.dirname;
        }))
        .pipe(zip(build.codeVersion + '.zip'))
        .pipe(gulp.dest('package/'));
});


var commands = {};
commands.login = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' -c dw_cookie.txt -d "LoginForm_Login=' + build.username + '&LoginForm_Password='+ build.password +  '&LoginForm_RegistrationDomain=Sites" https://' + build.username + ':' + build.password + '@' + build.hostname + '/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin';
commands.upload = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' -T package/' + build.codeVersion + '.zip https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/';
commands.unzip = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' -d method=UNZIP https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/' + build.codeVersion + '.zip';
commands.remove_remote_zip = 'curl --silent --show-error -k --request DELETE -u ' + build.username + ':' + build.password + ' https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/' + build.codeVersion + '.zip';
commands.activate_build = 'curl --silent --show-error -k -d "CodeVersionID=' + build.codeVersion + '" -b dw_cookie.txt https://' + build.hostname + '/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate';
commands.remove_archive_zip = 'rm /package/' + build.codeVersion + '.zip';
commands.login_2factor = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' -c dw_cookie.txt -d "LoginForm_Login=' + build.username + '&LoginForm_Password='+ build.password +  '&LoginForm_RegistrationDomain=Sites" --cert ' + build.certPath + ':' + build.certPass + ' --key ' + build.keyPath + ' https://' + build.username + ':' + build.password + '@' + build.hostname + '/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin';
commands.upload_2factor = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' --cert ' + build.certPath + ':' + build.certPass + ' --key ' + build.keyPath + ' -T package/' + build.codeVersion + '.zip https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/';
commands.unzip_2factor = 'curl --silent --show-error -k -u ' + build.username + ':' + build.password + ' --cert ' + build.certPath + ':' + build.certPass + ' --key ' + build.keyPath + ' -d method=UNZIP https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/' + build.codeVersion + '.zip';
commands.remove_remote_zip_2factor = 'curl --silent --show-error -k --request DELETE -u ' + build.username + ':' + build.password + ' --cert ' + build.certPath + ':' + build.certPass + ' --key ' + build.keyPath + ' https://' + build.hostname + '/on/demandware.servlet/webdav/Sites/Cartridges/' + build.codeVersion + '.zip';
commands.activate_build_2factor = 'curl --silent --show-error -k -d "CodeVersionID='+ build.codeVersion + '" -b dw_cookie.txt --cert ' + build.certPath + ':' + build.certPass + ' --key ' + build.keyPath + ' https://' + build.hostname + '/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate';
commands.remove_cookie = 'rm dw_cookie.txt';

gulp.task('exec:login', function(cb) {
    exec(commands.login, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:upload', function(cb) {
    exec(commands.upload, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});
gulp.task('exec:unzip', function(cb){
    exec(commands.unzip, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});
gulp.task('exec:remove_remote_zip', function(cb) {
    exec(commands.remove_remote_zip, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});
gulp.task('exec:activate_build', function(cb){
    exec(commands.activate_build, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:remove_archive_zip', function(cb){
    exec(commands.remove_archive_zip, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:login_2factor', function(cb) {
    exec(commands.login_2factor, function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:upload_2factor', function(cb) {
    exec(commands.upload_2factor,  function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});
gulp.task('exec:unzip_2factor', function(cb) {
    exec(commands.unzip_2factor,  function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:remove_remote_zip_2factor', function(cb) {
    exec(commands.remove_remote_zip_2factor,  function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});

gulp.task('exec:activate_build_2factor', function(cb) {
    if(build.activate != 'false' && build.activate !== false){
        exec(commands.activate_build_2factor,  function(err) {
            if (err) return cb(err); // return error
            cb(); // finished task);
        });
    }
});

gulp.task('exec:remove_cookie', function(cb) {
    exec(commands.remove_cookie,  function(err) {
        if (err) return cb(err); // return error
        cb(); // finished task);
    });
});
gulp.task('foo', function(){
    console.log(build.hostname);
    console.log(build.username);
    console.log(build.password);
    console.log(build.codeVersion);
    console.log(build.twofactor);
    console.log(build.activate);
    console.log(build.certPath);
    console.log(build.certPass);
});

//Run all tasks
var runSequence = require('run-sequence');
gulp.task('build:deploy', function(callback) {
    //runSequence('exec:activate_build_2factor', callback);
    runSequence('foo','exec:login_2factor','exec:upload_2factor','exec:unzip_2factor','exec:remove_remote_zip_2factor','exec:activate_build_2factor', callback);
});


//Clean packages 
gulp.task('build:cleanpackages', function (cb) {
    return del([
      './package/*',
    ]);
});